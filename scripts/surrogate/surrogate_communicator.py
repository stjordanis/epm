#!/usr/local/bin/python2.7
# encoding: utf-8
'''
EPM -- empirical performance models

@author:     Katharina Eggensperger and Marius Lindauer

@copyright:  2015 AAD Group Freiburg. All rights reserved.

@license:    GPLv2

@contact:    {eggenspk,lindauer}@cs.uni-freiburg.de
'''
import logging
import os
import socket
import sys
import time

import epm.surrogates.surrogate_remote as states

logger = logging.getLogger("Communicator")


def main(args):
    if args[0] == "QUALITY":
        quality = True
        args = args.pop()
    else:
        quality = False

    # Set dur to a random value
    dur = 3

    # Keep seed
    seed = int(float(args[4]))

    # Find out Port
    if "PORTFILE" in os.environ:
        portfile = os.environ["PORTFILE"]
    else:
        raise ValueError("Please set $PORTFILE")

    with open(portfile, 'r') as fh:
        port = "".join(fh.readlines())
    ip, port = port.strip().split(":")
    port = int(float(port))

    print("Trying to connect to port %d" % port)
    request = " ".join(args)
    connection = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    connection.settimeout(10)
    ans = None
    ct = 5
    while ans is None and ct > 0:
        try:
            start = time.time()
            print "Trying to receive data"
            connection.connect((ip, port))
            connection.sendall("GET:%s" % request)
            ans = []
            while True:
                data = connection.recv(8192)
                if not data:
                    break
                ans.append(data)
            ans = ''.join(ans)
            logger.debug("Received: <%s>" % ans)
            dur = time.time() - start
        except socket.error as e:
            logger.critical("ERROR: %s" % request)
            logger.critical("MSG: %s" % e.message)
            logger.critical("%d trials left (%s:%d) .. Sleep 5sec" % (ct, str(ip), port))
            time.sleep(5)
            ans = None
        ct -= 1

    if ans is None:
        logger.critical("No answer, sorry")
        sys.exit(1)

    ans = ans.split(":")
    if len(ans) == 2:
        # So far, so good
        state, ans = ans
        if state == states.SUCCESS:
            val, additional = ans.split(";")
            runtime = float(val)
            if additional == "CUTOFF":
                status = "TIMEOUT"
                logger.critical("Prediction was cut off")
            else:
                if args[1] == "SATISFIABLE":
                    status = "SAT"
                elif args[1] == "UNSATISFIABLE":
                    status = "UNSAT"
                else:
                    status = "SAT"
        else:
            logger.critical("ERROR: %s:%s" % (state, ans))
            sys.exit(1)
    else:
        ans = ":".join(ans)
        logger.critical("'ans' could not be parsed: %s\n" % ans)
        sys.exit(1)

    # Result for ParamILS: <solved>, <runtime>, <runlength>, <best sol>, <seed>
    #     Result for SMAC: <status>, <runtime>, <runlength>,  <quality>, <seed>
    if not quality:
        ret_str = "Result for ParamILS: %s, %s, 1, 1, %s, " \
                  "%s#%s:%d" % (status, str(runtime), str(seed), portfile, ip,
                                port)
    else:
        ret_str = "Result for ParamILS: %s, %s, 1, %s, %s, " \
                  "QUALITY:%s#%s:%d" % (status, str(dur), str(runtime), str(seed),
                                portfile, ip, port)

    print
    print ret_str
    print

if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    main(sys.argv[1:])
