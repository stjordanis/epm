#!/usr/local/bin/python2.7
# encoding: utf-8
'''
EPM -- empirical performance models

@author:     Katharina Eggensperger and Marius Lindauer

@copyright:  2015 AAD Group Freiburg. All rights reserved.

@license:    GPLv2

@contact:    {eggenspk,lindauer}@cs.uni-freiburg.de
'''
import logging
import os
import sys
import time

import epm.surrogates.surrogate_model_external as sme

logger = logging.getLogger("ExternalSurrogateCommunicator")


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)

    env_name = "MODELPKL"
    if env_name not in os.environ:
        logger.critical("%s is not in environ" % env_name)
        raise EnvironmentError("%s is not in environ" % env_name)

    surrogate = sme.SurrogateModelExtern(os.environ[env_name])
    pred, additional = surrogate.predict_from_message(" ".join(sys.argv[1:]))
    if additional == "CUTOFF":
        status = "TIMEOUT"
        logger.critical("Prediction was cut off")
    else:
        status = "SAT"

    seed = int(float(sys.argv[5]))
    pred = round(pred, 6)

    ret_str = "Result for ParamILS: %s, %s, 1, 1, %s, %s" % (status, str(pred), str(seed), additional)

    print
    print ret_str
    print
