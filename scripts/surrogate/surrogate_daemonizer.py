#!/usr/local/bin/python2.7
# encoding: utf-8
'''
EPM -- empirical performance models

@author:     Katharina Eggensperger and Marius Lindauer

@copyright:  2015 AAD Group Freiburg. All rights reserved.

@license:    GPLv2

@contact:    {eggenspk,lindauer}@cs.uni-freiburg.de
'''
import argparse
import os
import numpy
import time

from epm.surrogates import surrogate_model

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Start/Stop daemon')
    parser.add_argument('--pkl', dest='pkl', default=None, required=True,
                        help='Load model from here')
    parser.add_argument('--dir', dest='dir', default="/tmp", required=False,
                        help="Store daemon related files here")
    parser.add_argument('--pid', dest='pid', default=None, required=True,
                        help='Unique number/string')
    parser.add_argument('--idle', dest="idle", default=60*10, required=False,
                        type=int, help="Idle time before surrogate daemon dies")
    parser.add_argument('--debug', dest="debug", default=False,
                        action="store_true", help="Show some more output")
    parser.add_argument('--imputation', dest="imputation", default="def",
                        help="How to impute nonactive parameter")
    parser.add_argument('--quality', dest="quality", default=False,
                        action="store_true", help="Handle quality data?")
    ex = parser.add_mutually_exclusive_group(required=True)
    ex.add_argument('--start', dest='start', default=False, action="store_true",
                    help='Start daemon')
    ex.add_argument('--stop', dest='stop', default=False, action="store_true",
                    help='Stop daemon')
    ex.add_argument('--status', dest='status', default=False,
                    action="store_true", help='Show status daemon')

    args, unknown = parser.parse_known_args()

    if not os.path.isdir(args.dir):
        raise ValueError("%s is not a dir" % args.dir)
    args.dir = os.path.abspath(args.dir)

    pidfile = os.path.join(args.dir, "daemon_%s.pid" % args.pid)
    if "PORTFILE" in os.environ:
        portfile = os.environ["PORTFILE"]
    else:
        raise ValueError("Please set $PORTFILE")

    if args.start and os.path.isfile(pidfile):
        raise ValueError("File %s already exists" % pidfile)

    pkl_model = os.path.abspath(args.pkl)

    assert os.path.exists(pkl_model), \
        ".pkl file does not exist %s, cannot start daemon" % \
        pkl_model

    kw = {
        "pidfile": pidfile,
        "root": False,
        "stdout": "%s.out" % pidfile,
        "stderr": "%s.err" % pidfile,
        "daemonize": True
    }
    # alternatively: MyDaemon("/tmp/example-daemon.pid")
    daem = surrogate_model.SurrogateModel(**kw)

    if args.start:
        print "Start daemon: %s" % kw["daemonize"]
        it = 0
        while not daem.status() and it < 10:
            daem.start(pkl_filename=pkl_model, idle_time=args.idle,
                       portfile=portfile, debug=args.debug,
                       dtype=numpy.float32, impute_with=args.imputation,
                       quality=args.quality)
            time.sleep(10)
            it += 1
        if not daem.status():
            print 'Could not bring daemon to life! :('

    elif args.stop:
        daem.stop()
    elif args.status:
        if daem.status():
            print 'Daemon alive! :)'
        else:
            print 'Daemon in %s shows no sign of life! :(' % args.dir
