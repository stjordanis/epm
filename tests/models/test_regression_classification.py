'''
Created on Mar 27, 2015

@author: Katharina Eggensperger
'''
import logging
import unittest
import os
import cPickle

import numpy
from sklearn.cross_validation import KFold
from sklearn.metrics import mean_squared_error

import epm.preprocessing.pre_feature
import epm.models.classification_regression as classreg
import epm.reader.feature_parser
import epm.reader.performance_parser
import epm.pcs.config_space


logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger("bootstrapModelTest")

class ClassificationRegressionTest(unittest.TestCase):

    def setUp(self):
        self.rng = 1
        self.rs = numpy.random.RandomState(self.rng)
        self.src_dir = os.path.dirname(os.path.dirname(__file__))
        self.nqueens_features = os.path.join(self.src_dir, "files", "csv",
                                             "nqueens-features.csv")
        self.model_fl = os.path.join(self.src_dir, "classReg.mdl.pkl")
        self.cs_file = os.path.join(self.src_dir, "files", "pcs",
                                "clasp-sat-params-nat.pcs")
        self.cs = epm.pcs.config_space.ConfigSpace(self.cs_file,
                                                   normalize=False)

    def test_invalidInput(self):
        m = classreg.ClassificationRegression(rng=self.rng, debug=True,
                                              cutoff=1.5)
        y = self.rs.randn(100, 1).flatten()
        X = numpy.vstack([y**2, y+7, self.rs.randn(100, 1).flatten()]).T

        X = X.astype(numpy.float64)
        self.assertRaises(ValueError, m.fit, X=X, y=y)
        X = X.astype(numpy.float32)

        X[1, 1] = numpy.nan
        self.assertRaises(ValueError, m.fit, X=X, y=y)

        X[1, 1] = numpy.inf
        self.assertRaises(ValueError, m.fit, X=X, y=y)

        X[1, 1] = None
        self.assertRaises(ValueError, m.fit, X=X, y=y)


    def test_regression(self):
        y = self.rs.randn(100, 1).flatten()
        X = numpy.vstack([y**2, y+7, self.rs.randn(100).flatten()]).T

        # Add some TIMEOUTS
        idx = self.rs.randint(low=0, high=100, size=20)
        y = y.astype(numpy.float32)
        X = X.astype(numpy.float32)
        old_pred = None

        for cutoff in [10, 100, 1000]:
            m = classreg.ClassificationRegression(rng=self.rng, debug=True, cutoff=cutoff)
            y[idx] = cutoff

            m.fit(X=X, y=y)
            test_data = X[0]
            pred = m.predict(test_data)[0]
            self.assertAlmostEqual(first=1.6206072, second=pred, delta=0.0001)

            if old_pred is not None:
                self.assertAlmostEqual(old_pred, pred)
                old_pred = pred
            pred = m.predict(X[2])
            self.assertAlmostEqual(cutoff, pred)

    def test_real_data(self):
        fp = epm.reader.feature_parser.FeatureParser()
        inst_feat_dict, n_feats_used = fp.read_features(
                feature_file=self.nqueens_features, n_feats=1151)

        fpre = epm.preprocessing.pre_feature.PreprocessingFeatures(inst_feats_dict=inst_feat_dict)
        inst_feat_dict = fpre.normalization(inst_feats_dict=inst_feat_dict)

        data_file = os.path.join(self.src_dir, "files", "csv",
                                 "clasp_queens_cssc14_target_algo_runs.csv")

        pp = epm.reader.performance_parser.PerformanceParser(cutoff=300, par=10)
        config_list, perf_list, instance_list, success_list, cen_list = \
            pp.read_data(data_file)

        config_matrix = numpy.zeros((len(config_list),
                                     self.cs._n_params+n_feats_used),
                                    dtype=numpy.float32)
        for indx, config in enumerate(config_list):
            config_vec = self.cs.convert_param_dict(config)
            imputed_vec = self.cs.impute_non_active(config_vec, value="def")
            config_matrix[indx] = numpy.\
                hstack((imputed_vec, inst_feat_dict[instance_list[indx]]))
        perf_list = numpy.log10(numpy.array(perf_list).flatten())

        kf = KFold(config_matrix.shape[0], n_folds=20)
        rmse = list()
        max_y = 300
        #ct = 1
        for train, test in kf:
            x_train = config_matrix[train]
            y_train = perf_list[train]

            m = classreg.ClassificationRegression(rng=self.rng, debug=False,
                                                  cutoff=numpy.log10(max_y))
            m.fit(x_train, y_train)

            x_test = config_matrix[test]
            y_test = perf_list[test]
            pred = m.predict(x_test)

            score = numpy.sqrt(mean_squared_error(y_pred=pred, y_true=y_test))
            rmse.append(score)
            #print "%d RMSE: %f" % (ct, score)
            #ct += 1
        self.assertLess(numpy.mean(rmse), 2)

    def pickle_test(self):
        y = self.rs.randn(100, 1).flatten()
        X = numpy.vstack([y**2, y+7, self.rs.randn(100).flatten()]).T
        y = y.astype(numpy.float32)
        X = X.astype(numpy.float32)

        m = classreg.ClassificationRegression(rng=self.rng, debug=False,
                                              cutoff=2)
        m.fit(X=X[1:,:], y=y[1:])
        test_data = X[0]
        a_pred = m.predict(test_data)[0]

        # Now pickle model
        cPickle.dump(obj=m,
                     file=file(self.model_fl, 'w'),
                     protocol=cPickle.HIGHEST_PROTOCOL)

        del m

        m1 = cPickle.load(file(os.path.join(self.src_dir, "classReg.mdl.pkl")))
        b_pred = m1.predict(test_data)[0]
        self.assertAlmostEqual(a_pred, b_pred)

    def tearDown(self):
        if os.path.exists(os.path.join(self.src_dir, "classReg.mdl.pkl")):
            os.remove(os.path.join(self.src_dir, "classReg.mdl.pkl"))

if __name__ == "__main__":
    unittest.main()
