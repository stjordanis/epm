'''
Created on Mar 23, 2015

@author: lindauer
'''
import unittest
import random
import time
import os
import numpy

from epm.optimizer.ils import IteratedLocalSearch
from epm.pcs.config_space import ConfigSpace


class Test(unittest.TestCase):

    def setUp(self):
        random.seed(12345)
        #logging.basicConfig(level=logging.DEBUG)
        
    def testILS_Clasp(self):
        
        
        ils = IteratedLocalSearch()
        src_dir = os.path.dirname(os.path.dirname(__file__))
        pcs_file = os.path.join(src_dir, "files", "pcs",
                                "clasp-sat-params-nat.pcs")
        cs = ConfigSpace(pcs_file, normalize=True)
        
        func = lambda x : numpy.random.rand(len(x.shape))
        
        t0 = time.time()
        ils.optimize(func, cs, numpy.array([1]).reshape([1,1]))
        print("time (sec): %f" %(time.time() - t0))


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testILS']
    unittest.main()