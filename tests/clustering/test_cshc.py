'''
Created on Jun 9, 2015

@author: manju
'''
import functools
import unittest
import logging
import os
import sys
import time
import random

#sys.path.append(".")

import numpy

from sklearn.ensemble import RandomForestRegressor

from epm.experiment_utils.data_handling import load_data, separate_data_with_bools
from epm.preprocessing.imputor_missing_y import ImputorY
from epm.models import bootstrapModel
from epm.clustering.cshc import CSHC 
from epm.pcs.config_space import ConfigSpace
from epm.optimizer.ils import IteratedLocalSearch


class Test(unittest.TestCase):

    def setUp(self):
        logging.basicConfig(level=logging.DEBUG)
        #logging.basicConfig(level=logging.INFO)
        #logging.basicConfig(level=5)
        self.src_dir = os.path.dirname(os.path.dirname(__file__))

        self.data_dir = os.path.join(self.src_dir, "..", "data", "cssc14_nqueens_clasp")
        self.features = os.path.join(self.data_dir, "nqueens-features.csv")
        self.pcs = os.path.join(self.data_dir, "clasp-sat-params-nat.pcs")
        self.perf = os.path.join(self.data_dir, "clasp_queens_cssc14_target_algo_runs.csv")
        
        self.data_dir = os.path.join(self.src_dir, "..", "data", "cssc14_K3_minisathack")
        self.features = os.path.join(self.data_dir, "K3.features.min")
        self.pcs = os.path.join(self.data_dir, "minisat_HACK_999ED_CSSC-params.pcs")
        self.perf = os.path.join(self.data_dir, "target_algo_runs.csv")
        
        random.seed(12345)
        
    def test_cshc(self):
        model_debug=False

        rf = functools.partial(RandomForestRegressor, random_state=1)
        brf = functools.partial(bootstrapModel.bootstrapModel, rng=1,
                             debug=model_debug, n_bootstrap_samples=10,
                             bootstrap_size=0.7, base_estimator=rf)

        encoded_matrix, inst_feat_dict, inst_list, n_feats_used, perf_list, suc_list, cen_list = \
            load_data(self.perf, feature_file=self.features, pcs_file=self.pcs, max_y=300)
        cs = ConfigSpace(self.pcs)

        n_data = 300
        encoded_matrix = encoded_matrix[:n_data]
        inst_list = inst_list[:n_data]
        perf_list = perf_list[:n_data]
        #suc_list = suc_list[:n_data]
        cen_list = cen_list[:n_data]

        feat_matrix = []
        for inst_ in inst_list[:n_data]:
            feat_matrix.append(inst_feat_dict[inst_])
        feat_matrix = numpy.array(feat_matrix)

        logging.info("### Imputation")
        # Impute data for training folds
        imputor = ImputorY(debug=False)
        perf_list = imputor.impute_y(y=perf_list,
                                     is_censored=cen_list,
                                     max_y=300,
                                     configs_list=encoded_matrix,
                                     inst_list=inst_list,
                                     inst_feature_dict=inst_feat_dict,
                                     model=brf, log=True)

        # Put performance data on logscale
        logging.info("### Using logscale for train")
        log_imp_train_y = numpy.log(perf_list)
        X_train = numpy.hstack((encoded_matrix,feat_matrix))

        # Fit a model
        logging.info("### Start training")
        m = rf()
        m.fit(X_train, log_imp_train_y)
        
        ils = IteratedLocalSearch()
        
        logging.info("### Start Clustering")
        clustering = CSHC()
        t0 = time.time()
        clusters = clustering.cluster(inst_feat_dict=inst_feat_dict, 
                                      optimizer=ils, 
                                      model=m, 
                                      pcs=cs, 
                                      min_cluster_size=10,
                                      split = 1) 
        logging.info("Time CSHC: %.2f" %(time.time() - t0))
        logging.info(map(lambda x : len(x), clusters))
        #print(clusters)

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()