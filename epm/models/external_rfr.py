'''
Created on October 07, 2015

@author: Marius Lindauer
'''

import logging

import numpy

from pyrfr import regression32 as regression


class ExternalRFR(object):

    def __init__(self,
                 cs,
                 n_feats,
                 do_bootstrapping=True,
                 min_samples_to_split=2,
                 min_samples_in_leaf=1,
                 max_depth=0,
                 max_num_nodes=0,
                 epsilon_purity=1e-8,
                 num_trees=10,
                 seed=12345,
                 frac_points_per_tree=0,
                 ratio_features=0,
                 debug=False, dtype=numpy.float32):
        """
        
        We assume that x is encoded as config||features

        :param cs: object of ConfigSpace
        :param n_feats: number of instance features (int)
        :param do_bootstrapping: bootstrapping? (bool)
        :param num_data_points_per_tree: number of data points per tree (int)
        :param max_features_per_split: maximal number of features per split (int)
        :param min_samples_to_split: minimal number of samples to split a node (int)
        :param min_samples_in_leaf: minimal number of samples in leafs after a split (int)
        :param max_depth:  maximal depth of a tree (int)
        :param epsilon_purity: purity of leafs (double)
        :param num_trees: number of trees (int)
        :param seed: random seed (int)
        :param debug: debug output (bool)
        :return: self
        """

        self.logger = logging.getLogger("externalRFR")
        self.debug = debug
        if self.debug:
            self.logger.setLevel(logging.DEBUG)
        else:
            self.logger.setLevel(logging.INFO)

        # These parameters cannot be set directly
        self.frac_points_per_tree = frac_points_per_tree
        self.ratio_features = ratio_features

        # logging.info("Use %s as EPM model" % (name))
        model = regression.binary_rss()
        model.seed = seed
        model.do_bootstrapping = do_bootstrapping
        model.min_samples_to_split = min_samples_to_split
        model.min_samples_in_leaf = min_samples_in_leaf
        model.max_depth = max_depth
        model.max_num_nodes = max_num_nodes
        model.epsilon_purity = epsilon_purity
        model.num_trees = num_trees
        self.model = model

        types_params = numpy.array(cs._cat_size)
        types_feats = numpy.zeros(n_feats, dtype=numpy.uint64)
        self.logger.debug("Types params shape: %s" % (str(types_params.shape)))
        self.logger.debug("Types feats shape: %s" % (str(types_feats.shape)))
        types = numpy.hstack((types_params, types_feats))
        self.logger.debug("Types shape: %s" % (str(types.shape)))
        self.logger.debug("Types: %s" % (str(types)))

        self.types = numpy.array(types, dtype=numpy.uint64)

        self._dtype = dtype

    def __getstate__(self):
        d = dict(self.__dict__)
        del d['logger']
        return d

    def __setstate__(self, d):
        self.__dict__.update(d)
        self.logger = logging.getLogger("externalRFR_resurrected")

        if self.debug:
            logging.basicConfig(level=logging.DEBUG)
        else:
            logging.basicConfig(level=logging.INFO)

    def fit(self, X, y):
        """
        Trains a random forest regression on X
        :param X: numpy array mxn, required, features
        :param y: numpy array m, required, objective value
        :return: self
        """

        data1 = regression.numpy_data_container(X, y, self.types)
        self.model.num_data_points_per_tree = \
            max(0, int(X.shape[0]*self.frac_points_per_tree))
        self.model.max_features = max(0, int(X.shape[1]*self.ratio_features))
        self.model.fit(data1)

        return self

    def change_type_array_to_outlier_imputation(self):
        self.logger.critical("Change type array to handle 'outlier' "
                             "feature imputation")
        self.types = numpy.array([int(i+1) if i != 0 else i for i in self.types],
                                 dtype=numpy.uint64)

    def predict_mean_std(self, X):
        """
        returns mean/var prediction
        :param x: numpy vector/matrix, required, features
        :return: mean, variance 
        """
        mean, var = self.predict_mean_var(X)
        std = numpy.sqrt(var)

        return mean, std

    def predict_mean_var(self, X):
        """
        returns mean/sqrt(var) prediction
        :param x: numpy vector/matrix, required, features
        :return: mean, variance 
        """
        if self.model is None:
            raise ValueError("Model is not yet trained")

        threshold = 10 ** -10
        if len(X.shape) > 1:
            pred = numpy.array([self.model.predict(x) for x in X])
            mean = pred[:, 0]
            var = pred[:, 1]
            var[var < threshold] = threshold
            var[numpy.isnan(var)] = threshold

            mean = numpy.array(mean, dtype=self._dtype)
            var = numpy.array(var, dtype=self._dtype)
        else:
            mean, var = self.model.predict(X)
            if var < threshold:
                self.logger.debug("Standard deviation is %f, capping to 10^-5" % var)
                var = threshold
            var = numpy.array([var, ], dtype=self._dtype)
            mean = numpy.array([mean, ], dtype=self._dtype)

        return mean, var

    def predict(self, X):
        """
        exists to be compatible to sklearn estimators
        :param x: numpy vector/matrix, required, features
        :return: mean prediction
        """
        return self.predict_mean_std(X=X)[0]

    def out_of_bag_error(self):
        return self.model.out_of_bag_error()
