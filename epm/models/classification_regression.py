'''
Created on Jan 15, 2016

@author: Katharina Eggensperger
'''

import logging

import numpy

from sklearn.ensemble import RandomForestClassifier, RandomForestRegressor


class ClassificationRegression(object):

    def __init__(self, rng, cutoff, regr_args=None, class_args=None, debug=False):
        """

        :param rng: Random number
        :param regr_args: dict with additional arguments for the regressor
        :param class_args: dict with additional arguments for the classifier
        :param cutoff: int, required
        :param debug:
        :return: self
        """

        self.rng = rng
        self._dtype = numpy.float32
        self.rs = numpy.random.RandomState(seed=rng)
        self.cutoff = cutoff

        if regr_args is None:
            self.regr_args = dict()
        else:
            self.regr_args = regr_args
        if "random_state" not in self.regr_args:
            self.regr_args["random_state"] = self.rng

        if class_args is None:
            self.class_args = dict()
        else:
            self.class_args = class_args
        if "random_state" not in self.class_args:
            self.class_args["random_state"] = self.rng

        self.regr_model = None
        self.class_model = None

        self.logger = logging.getLogger("classification_regression")
        self.debug = debug
        if self.debug:
            logging.basicConfig(level=logging.DEBUG)
        else:
            logging.basicConfig(level=logging.INFO)

    def __getstate__(self):
        d = dict(self.__dict__)
        del d['logger']
        return d

    def __setstate__(self, d):
        self.__dict__.update(d)
        self.logger = logging.getLogger("classification_regression")
        if self.debug:
            logging.basicConfig(level=logging.DEBUG)
        else:
            logging.basicConfig(level=logging.INFO)

    def fit(self, X, y):
        """
        Trains a classification forest on X to classify timeouts
        then trains a regression tree to predict runtimes
        :param X: numpy array mxn, required, features
        :param y: numpy array m, required, objective value
        :return: self
        """
        self._check_before_fit(X, y)

        y_binary = numpy.zeros(y.shape)
        y_binary[y >= self.cutoff] = 1

        logging.debug("Found %d of %d TIMEOUTS (cutoff=%d)" % (sum(y_binary),
                                                               y_binary.shape[0],
                                                               self.cutoff))
        if sum(y_binary) == 0:
            self.logger.critical("No TIMEOUTS found")
            raise ValueError("No TIMEOUTS found")

        # Fit classification model
        self.class_model = RandomForestClassifier(**self.class_args)
        self.class_model.fit(X, y_binary)

        # Remove TIMEOUTS from trainingdata
        mask = numpy.array([True] * y.shape[0], dtype=bool)
        mask[numpy.where(y_binary)] = False
        X_regr = X[mask, :]
        y_regr = y[mask]

        # Fit regression model
        self.regr_model = RandomForestRegressor(**self.regr_args)
        self.regr_model.fit(X_regr, y_regr)

        return self

    def _check_before_fit(self, X, y):
        # Might be better to do some checks
        assert isinstance(X, numpy.ndarray)
        assert isinstance(y, numpy.ndarray)

        if X.dtype != numpy.float32:
            raise ValueError("X has dtype %s, expected %s" %
                             (X.dtype, self._dtype))
        if y.dtype != numpy.float32:
            raise ValueError("y has dtype %s, expected %s" %
                             (y.dtype, self._dtype))
        assert X.shape[0] == len(y)

        if not numpy.isfinite(X).all():
            logging.critical("X array is not finite")
            raise ValueError("X array is not finite")
        if not numpy.isfinite(y).all():
            logging.critical("y array is not finite")
            raise ValueError("y array is not finite")

    def _check_before_predict(self, X):
        if self.regr_model is None or self.class_model is None:
            raise ValueError("Model is not yet trained")

        assert type(X) == numpy.ndarray
        assert X.ndim == 2

    def _prepare_output(self, X):
        X = X.astype(numpy.float32)
        if len(X.shape) == 1:
            return X
        else:
            self.logger.debug("Found array with shape %s, return flattened" % str(X.shape))
            return X.flatten()

    def predict(self, X):
        """
        exists to be compatible to sklearn estimators
        :param X:
        :return: mean prediction
        """

        pred_y = self.class_model.predict(X)

        self.logger.debug("Predict %d/%d as TIMEOUT" %
                          (sum(pred_y), pred_y.shape[0]))

        mask = numpy.array([False] * pred_y.shape[0], dtype=bool)
        mask[pred_y == 1] = True

        pred_y = self.regr_model.predict(X)
        pred_y[numpy.where(mask)] = self.cutoff

        return self._prepare_output(pred_y)