'''
Created on Apr 15, 2015

@author: Katharina Eggensperger
'''

import logging
import random
import sys
import time

import GPy

import epm.models.external_GP

logger = logging.getLogger("externalSparseGP")


class externalSparseGP(epm.models.external_GP.externalGP):

    def fit(self, X, y):
        """
        Trains a GP on X
        :param X: numpy array mxn, required, features
        :param y: numpy array m, required, objective value
        :return: self
        """
        if self.debug:
            logger.setLevel(logging.DEBUG)

        # We only want one output dimension
        if y.ndim == 1:
            y = y.reshape([y.shape[0], 1])

        self.input_dim = X.shape[1]
        self.kern_args['input_dim'] = self.input_dim
        self.num_inducing_points = 300
        self.iterations = 10
        if X.shape[0] < self.num_inducing_points:
            self.iterations = 1
            self.num_inducing_points = X.shape[0]

        """
        cur_best_lklhd = -sys.maxint
        cur_best_kernel = None
        cur_best_model = None
        for i in range(self.iterations):
            start = time.time()
            kernel = self.kern(**self.kern_args)
            subset = sorted(random.sample(population=xrange(X.shape[0]),
                                          k=self.num_inducing_points))
            Z = X[subset, :]
            model = GPy.models.SparseGPRegression(X=X, Y=y, Z=Z,
                                                  kernel=kernel)
            logger.debug("Constrain Positive")
            model.constrain_positive('')
            model.Z.fix()
            #logger.info("Model before opt: %s" % str(model))
            #logger.info("Inducing points [0]: %s" % str(model.Z[0]))
            model.optimize()
            #logger.info("Model after opt: %s" % str(model))
            #logger.info("Inducing point [0]: %s" % str(model.Z[0]))
            lkhd = model.log_likelihood()
            if lkhd > cur_best_lklhd:
                logger.debug("Found better likelihood %f > %f" % (lkhd, cur_best_lklhd))
                cur_best_lklhd = lkhd
                cur_best_model = model
                cur_best_kernel = kernel
            dur = time.time() - start
            logger.debug("Iteration %d took %fsec, likelihood: %f" % (i, dur, lkhd))
        self.model = cur_best_model
        self.kernel = cur_best_kernel
        """

        self.kernel = self.kern(**self.kern_args)
        self.model = GPy.models.SparseGPRegression(X=X, Y=y,
                                                   num_inducing=self.num_inducing_points,
                                                   kernel=self.kernel)
        self.model.constrain_positive('')
        #self.optimizer = GPy.inference.optimization.SCG
        #self.model.preferred_optimizer = GPy.inference.optimization.SCG
        self.model.optimize_restarts(num_restarts=self.restarts, messages=self.debug) #, optimizer="SCG", max_iters=50)

        logger.info("model after opt: %s" % str(self.model))
        logger.debug("lengthscale: %s" % str(self.model.rbf.lengthscale))

        return self