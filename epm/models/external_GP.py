'''
Created on Mar 27, 2015

@author: Katharina Eggensperger
'''

import logging
import sys

import numpy
import random

import GPy

logger = logging.getLogger("externalGP")


class externalGP(object):

    def __init__(self, rng, kern=None, kern_args=None, debug=False,
                 max_value=sys.maxint, num_restarts=10):
        """

        :param rng: Random number
        :param kern: GPy.kern object, else GPy.kern.matern52 is used
        :param kern_args: dict with additional arguments for the kern
        :param debug:
        :return: self
        """

        if debug:
            logger.setLevel(logging.DEBUG)

        self.rng = rng
        self.debug = debug
        self.kern = kern
        self.kern_args = kern_args
        self.kernel = None
        self.model = None
        self.max_value = max_value
        self.restarts = num_restarts
        if kern is None:
            logger.debug("Using RBF kernel")
            logger.info("kern is None: using RBF, ignoring kern_args")
            self.kern = GPy.kern.RBF
            self.kern_args = {'variance': 1.0, 'lengthscale': None, 'ARD': True}
        logger.debug("%s" % str(self.model))
        numpy.random.seed(rng)
        random.seed(rng)
        self.input_dim = None

    def fit(self, X, y):
        """
        Trains a GP on X
        :param X: numpy array mxn, required, features
        :param y: numpy array m, required, objective value
        :return: self
        """
        # We only want one output dimension
        if y.ndim == 1:
            y = y.reshape([y.shape[0], 1])

        # limit data to first 2000 points
        max_points = 2000
        if max_points < X.shape[0]:
            subset = sorted(random.sample(xrange(X.shape[0]), max_points))
            X = X[subset, :]
            y = y[subset]

        self.input_dim = X.shape[1]
        self.kern_args['input_dim'] = self.input_dim
        self.kernel = self.kern(**self.kern_args)
        logger.info("model before opt: %s" % str(self.model))

        self.model = GPy.models.GPRegression(X, y, self.kernel)

        self.model.constrain_positive('')
        #logger.info("Using some very critical mcmc optimization method")
        #mcmc =  GPy.inference.mcmc.HMC(model=self.model, stepsize=0.01)
        self.model.optimize_restarts(num_restarts=self.restarts,
                                     messages=self.debug)
        logger.info("model after opt: %s" % str(self.model))
        return self

    def _check_before_predict(self, X):
        if self.model is None:
            raise ValueError("Model is not yet trained")

        assert type(X) == numpy.ndarray
        assert X.ndim == 2

    def _prepare_output(self, X):
        # We don't want anything larger than max_value
        X = [self.max_value if x > self.max_value else x for x in X]

        X = numpy.array(X)
        if len(X) > 1 and min(X) == max(X):
            logger.debug("Output is broken, GPS predicts data mean")
        if len(X.shape) == 1:
            return X
        else:
            #logger.critical("Found a array with shape %s, return flattened" % str(X.shape))
            return X.flatten()

    def predict_mean_var(self, X):
        """
        returns mean/var prediction
        :param X: numpy array mxn, required, features
        :return: mx1
        """
        self._check_before_predict(X)

        mean, var = self.model.predict(X)
        
        mean = self._prepare_output(X=mean)
        var = self._prepare_output(X=var)

        if min(var) < 10**-5:
            logger.debug("Variance is small, capping to 10^-5")
            var = [10**-5 if i < 0 else i for i in var]

        var = self._prepare_output(X=var)

        return mean, var

    def predict_mean_std(self, X):
        """
        returns mean/sqrt(var) prediction
        :param X: numpy array mxn, required, features
        :return: mx1
        """
        mean, var = self.predict_mean_var(X)
        #logger.critical("MEAN: %s" % str(var))
        var = numpy.sqrt(var)

        return mean, var

    def predict(self, X):
        """
        exists to be compatible to sklearn estimators
        :param X:
        :return: mean prediction
        """
        return self.predict_mean_var(X=X)[0]

    def normal_sample_predict(self, X):
        """
        Calls predict and samples from a normal distribution
        :param X:
        :return: pred
        """
        mean, std = self.predict_mean_std(X)

        zero_mean = numpy.zeros(len(mean))
        # Yippieh
        logger.critical("mean: %s" % str(mean))
        logger.critical("var: %s" % str(std))
        pred = map(lambda m, s: numpy.random.normal(m, s), zero_mean, std)

        pred = [p+m for p, m in zip(pred, mean)]
        logger.critical("pred: %s" % str(pred))

        pred = numpy.array(pred)

        return pred

    def sample_from_posterior(self, X, num_samples=100):
        """
        returns posterior samples
        :param X: numpy array mxn, required, features
        :param num_samples: #samples to draw from GP
        :return: numpy array mxnum_samples
        """
        self._check_before_predict(X)

        # N x samples
        samples = self.model.posterior_samples(X, size=num_samples,
                                               full_cov=False)
        return samples

    def mean_std_posterior(self, X, num_samples=100):
        """
        returns posterior samples
        :param X: numpy array mxn, required, features
        :param num_samples: #samples to draw from GP
        :return: numpy array mxnum_samples
        """
        # N x samples
        samples = self.sample_from_posterior(X, num_samples=num_samples)
        mean = numpy.mean(samples, axis=1)
        std = numpy.std(samples, axis=1)
        return mean, std

    def normal_sample_posterior(self, X, num_samples=100):
        """
        returns posterior samples
        :param X: numpy array mxn, required, features
        :param num_samples: #samples to draw from GP
        :return: numpy array mxnum_samples
        """
        mean, std = self.mean_std_posterior(X, num_samples=num_samples)

        # Yippieh
        pred = map(lambda m, s: numpy.random.normal(m, s), mean, std)
        pred = numpy.array(pred)

        return pred