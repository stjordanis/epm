'''
Created on May 05, 2015

@author: Katharina Eggensperger
'''
import functools

#import GPy

from sklearn.ensemble import RandomForestRegressor, ExtraTreesRegressor
from sklearn.neighbors import KNeighborsRegressor
from sklearn.linear_model import LinearRegression, Ridge, BayesianRidge

from epm.models import bootstrapModel, external_rfr, external_rfr_predict,\
    external_rfr_quantile

# This is a collection of all possible models

RNG = 0

basic_models = {"brr": BayesianRidge,
                "lr": LinearRegression,
                "rf": functools.partial(RandomForestRegressor, n_estimators=10,
                                        random_state=RNG),  #, max_features="sqrt"),
                "et": functools.partial(ExtraTreesRegressor, random_state=RNG),
                "knn": KNeighborsRegressor,
                "rr": Ridge,
                "rfr": functools.partial(external_rfr.ExternalRFR,
                                         num_trees=48,
                                         seed=RNG,
                                         do_bootstrapping=False,
                                         frac_points_per_tree=0.635453473468,
                                         max_depth=26,
                                         max_num_nodes=16678,
                                         min_samples_in_leaf=1,
                                         min_samples_to_split=5,
                                         ratio_features=0.284881375208),
                "rfr_m": functools.partial(external_rfr.ExternalRFR,
                                           num_trees=48,
                                           seed=RNG,
                                           compute_oob_error=True,
                                           do_bootstrapping=True,
                                           frac_points_per_tree=0.66,
                                           max_depth=0,
                                           max_num_nodes=0,
                                           min_samples_in_leaf=1,
                                           min_samples_to_split=2,
                                           ratio_features=0.66),
                "rfr_predict": functools.partial(
                        external_rfr_predict.ExternalRFRPredict,
                        num_trees=48,
                        seed=RNG,
                        compute_oob_error=True,
                        do_bootstrapping=False,
                        frac_points_per_tree=0.635453473468,
                        max_depth=26,
                        max_num_nodes=16678,
                        min_samples_in_leaf=1,
                        min_samples_to_split=5,
                        ratio_features=0.284881375208),
                "rfr_quantile": functools.partial(
                        external_rfr_quantile.ExternalRFRQuantile,
                        num_trees=48,
                        seed=RNG,
                        do_bootstrapping=False,
                        frac_points_per_tree=0.8, #0.635453473468,
                        max_depth=26,
                        max_num_nodes=50000, #16678,
                        min_samples_in_leaf=1,
                        min_samples_to_split=5,
                        epsilon_purity=1e-8,
                        ratio_features=0.284881375208),
                "rfr_predict_m": functools.partial(
                    external_rfr_predict.ExternalRFRPredict,
                    num_trees=48,
                    seed=RNG,
                    do_bootstrapping=True,  # False,#true
                    frac_points_per_tree=0.66,  # 0.635453473468,#0.66
                    max_depth=0,  # 26, #0
                    max_num_nodes=0,  # 16678, #0
                    min_samples_in_leaf=1,
                    min_samples_to_split=2,  # 25, #2
                    ratio_features=0.66)  # 0.284881375208)#0.66
                }

bootstrap_settings = {"n_bootstrap_samples": 50,
                      "bootstrap_size": 0.7,
                      "debug": False,
                      "rng": RNG}

# keys = [name, predict model, imputation model
model_dict = {
    "br": ("BayesianRidgeRegression", basic_models["brr"],
           functools.partial(bootstrapModel.bootstrapModel,
                             base_estimator=basic_models["brr"],
                             **bootstrap_settings)),
    "lr": ("LinearRegression", basic_models["lr"],
           functools.partial(bootstrapModel.bootstrapModel,
                             base_estimator=basic_models["lr"],
                             **bootstrap_settings)),
    "rf": ("RandomForest", basic_models["rf"],
           functools.partial(bootstrapModel.bootstrapModel,
                             base_estimator=basic_models["rf"],
                             **bootstrap_settings)),
    "rfr": ("rfr", basic_models["rfr_predict"], basic_models["rfr"]),
    "rfrq": ("rfrq", basic_models["rfr_quantile"], basic_models["rfr"]),
    "et": ("ExtraTrees", basic_models["et"],
           functools.partial(bootstrapModel.bootstrapModel,
                             base_estimator=basic_models["et"],
                             **bootstrap_settings)),
    "knn": ("KnearestNeighbor", basic_models["knn"],
            functools.partial(bootstrapModel.bootstrapModel,
                              base_estimator=basic_models["knn"],
                              **bootstrap_settings)),
    "rr": ("RidgeRegression", basic_models["rr"],
           functools.partial(bootstrapModel.bootstrapModel,
                             base_estimator=basic_models["rr"],
                             **bootstrap_settings)),
    "rfrm": ("rfrm", basic_models["rfr_predict_m"], basic_models["rfr"]),
    "mrfrm": ("mrfrm", basic_models["rfr_predict_m"], basic_models["rfr_m"])
}
