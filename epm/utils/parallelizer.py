'''
Created on Jun 25, 2015

@author: lindauer
'''

import multiprocessing
import Queue
import logging

class Parallelizer(object):
    '''
       implements a parallel version of map (without pickle requirement)
    '''


    def __init__(self, threads):
        '''
        Constructor
        Args:
            threads: number of threads
            wait: seconds to wait for new input in output queue
        '''
        
        self._threads = threads
        
    def par_map(self, func, list_):
        '''
            applies func to each element of list and returns a list with the results
            ATTENTION: since it is an async map, the order of the returned is not necessarily the same as the input list
            ATTENTION(2): Not efficient for very fast function calls (func)
        '''
        
        queue_in = multiprocessing.Queue()
        queue_out = multiprocessing.Queue()
        
        for element in list_:
            queue_in.put(element)
            
        procs = [multiprocessing.Process(target = self.__par_func, args = (func, queue_in, queue_out)) for _ in xrange(self._threads)]
        
        for p in procs:
            p.start()
        #=======================================================================
        # for p in procs:
        #     p.join()
        #=======================================================================
         
        logging.debug("Collect results from threads")
        
        out_list = []
        
        for _ in xrange(0, len(list_)):
            res_ = queue_out.get()
            out_list.append(res_)
            
        assert len(out_list) == len(list_), "Error in par_map (mulit-threading): in and out list have not the same size"
            
        return out_list
            
    def __par_func(self, func, queue_in, queue_out):
        
        while True:
            try:
                element = queue_in.get_nowait()
            except Queue.Empty:
                break
            queue_out.put(func(element))
        
        logging.debug("Thread finished")
        
            