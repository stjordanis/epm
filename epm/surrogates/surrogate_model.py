'''
EPM -- empirical performance models

@author:     Katharina Eggensperger and Marius Lindauer

@copyright:  2015 AAD Group Freiburg. All rights reserved.

@license:    GPLv2

@contact:    {eggenspk,lindauer}@cs.uni-freiburg.de
'''

try:
    import cPickle as pickle
except ImportError:
    import pickle
import logging
import os
import socket
import time
import numpy

import mattdaemon

from epm.surrogates import surrogate_remote
from epm.experiment_utils.data_handling import warp, unwarp
from epm.models import external_rfr_predict, external_rfr_quantile


class SurrogateModel(mattdaemon.daemon):

    def run(self, *args, **kwargs):
        if "pkl_filename" not in kwargs:
            raise ValueError("'pkl_filename' is not in kwargs")
        else:
            assert os.path.exists(kwargs["pkl_filename"]), \
                "%s does not exist" % kwargs["pkl_filename"]

        if "idle_time" not in kwargs:
            raise ValueError("'idle_time' is not in kwargs")

        if "impute_with" not in kwargs:
            raise ValueError("'impute_with' is not in kwargs")

        self.quality = kwargs['quality']
        self.impute_with = kwargs['impute_with']

        self.debug = False
        if "debug" in kwargs:
            self.debug = True

        self.logger = logging.getLogger("SurrogateModel")

        if self.debug:
            logging.basicConfig(level=logging.DEBUG)
            self.logger.setLevel(logging.DEBUG)
        else:
            logging.basicConfig(level=logging.INFO)

        self.pkl_filename = kwargs["pkl_filename"]
        self.idle_time = int(float(kwargs["idle_time"]))

        if "portfile" in kwargs:
            self.portfile = kwargs["portfile"]
        else:
            raise ValueError("'portfile' not given")

        if "dtype" in kwargs:
            self._dtype = kwargs["dtype"]
        else:
            self._dtype = numpy.float32

        if os.path.exists(self.portfile):
            raise ValueError("%s already exists" % self.portfile)

        self.ip = "127.0.0.1"

        self.load_model()
        self.logger.critical("I am a well behaving daemon")
        self.startup()
        self.logger.debug("I work according to Pep-3143")
        self.logger.debug("Do I return quality? - %s" % str(self.quality))
        self.logger.debug("KWARGS: %s" % str(kwargs))
        with open(self.portfile, 'w') as fh:
            fh.write("%s:%d" % (self.ip, self.remote.port))
        self.logger.debug("I will listen to %s:%d" % (self.ip,
                                                      self.remote.port))

        self.main_loop()

    def load_model(self):
        m, inst_feat_dict, cs = pickle.load(open(self.pkl_filename, 'rb'))
        self.model = m
        self.inst_feat_dict = inst_feat_dict
        self.cs = cs
        self.connected = False
        self.remote = surrogate_remote.SurrogateRemote(ip=self.ip,
                                                       debug=self.debug)

        if isinstance(self.model, external_rfr_predict.ExternalRFRPredict) or \
                isinstance(self.model,
                           external_rfr_quantile.ExternalRFRQuantile):
            self.logger.critical("NO one-hot encoding")
            self.encode = False
        else:
            self.logger.critical("Activate one-hot encoding")
            self.encode = True

    def load_model_from_file(self):
        m, inst_feat_dict, cs = pickle.load(open(self.pkl_filename, 'rb'))
        self.model = m
        self.inst_feat_dict = inst_feat_dict
        self.connected = False

    def startup(self, trials=10):
        self.logger.debug("Start to connect..")
        for t in range(trials):
            suc = self.remote.start_socket()
            if suc:
                self.connected = True
                self.logger.debug("Success\n")
                break
            else:
                self.logger.critical("Failed .. sleep for 5sec")
                time.sleep(5)

    @staticmethod
    def convert_params_to_vec(param_str, cs, encode=True, impute_with="def"):
        ct = 0
        p_dict = dict()
        while ct < len(param_str)-1:
            key = param_str[ct].strip().strip("'")
            value = param_str[ct + 1].strip().strip("'")
            if key[0] != "-":
                # Something went wrong while parsing
                raise ValueError("Could not parse paramstring: %s" % param_str)
            key = key[1:]
            if key not in cs.parameters:
                # Unknown param
                raise ValueError("Couldn't find %s" % key)
            p_dict[key] = value
            ct += 2
        # Here normalization is done
        logging.critical("%s, [DICT] %s" % (cs.normalize, str(p_dict)))
        vec = cs.convert_param_dict(p_dict)
        vec = cs.impute_non_active(vec, value=impute_with)
        logging.debug("[VEC]: [%s]" % ",".join([("%f" % v) for v
                                                        in vec.flatten()]))
        # Convert to numpy array
        vec = numpy.array(vec).reshape([1, -1])
        if encode:
            # one-hot-encoding
            vec = cs.encode(vec)
        return vec

    def main_loop(self):
        if not self.connected:
            self.logger.critical("Not yet connected\n")
            return
        time_left = self.idle_time
        destroy = False
        self.logger.critical("I am in my main loop\n")
        self.logger.critical("I am listening to %s:%d\n" %
                             (self.remote.ip_address, self.remote.port))
        try:
            while not destroy:
                idle_start = time.time()
                if time_left <= 0:
                    destroy = True
                    self.logger.critical("I am bored .. goodbye\n")
                    continue
                try:
                    self.remote.connect(timeout=10)
                except socket.timeout:
                    # No connection received
                    time_left -= (time.time() - idle_start)
                    self.logger.critical("I'm so lonesome, I could die in %f "
                                         "sec" % time_left)
                    continue

                # Now we went somehow further and can reset the counter
                time_left = self.idle_time
                self.logger.debug("[CON]: %s\n" % self.remote._conn)
                data = self.remote.receive()
                self.logger.debug("[REC]: \n###\n%s\n###\n" % data)

                data = data.split(":")
                ans = "Not yet processed"
                state = surrogate_remote.FAIL

                # First split request
                if len(data) < 2:
                    state = surrogate_remote.FAIL
                    ans = "Wrong msg format"
                    self.send_answer(msg=ans, state=state)
                elif len(data) > 2:
                    key = data[0]
                    msg = ":".join(data[1:])
                else:
                    key = data[0]
                    msg = data[1]

                # Now find out what to do
                if key == "STOP":
                    ans = "Goodbye:%s" % msg
                    state = surrogate_remote.SUCCESS
                    destroy = True
                elif key == "HELLO":
                    # Dummy function to check state
                    ans = "FINE:%s" % msg
                    state = surrogate_remote.SUCCESS
                elif key == "GET":
                    try:
                        state, pred, additional = self.handle_request(msg=msg)
                        if pred is None:
                            ans = additional
                        else:
                            ans = "%f;%s" % (pred, additional)
                    except Exception, e:
                        state = surrogate_remote.FAIL
                        ans = "%s" % e.message
                else:
                    ans = "UNKNOWNKEY: '%s'" % key
                    state = surrogate_remote.FAIL

                self.send_answer(msg=ans, state=state)
        except (KeyboardInterrupt, SystemExit):
            self.shutdown()
            raise
        except Exception as e:
            self.logger.critical("\n" + "#"*80 + "\nIgnoring error: " +
                                 e.message + "\n" + "#"*80 + "\n\n")

    def handle_request(self, msg):
        request = msg.split(" ")
        instance_string = request[0]
        specific_info = request[1]          # ignored
        runtime_cutoff = float(request[2])
        runlength = request[3]       # ignored
        seed = int(float(request[4]))
        params = request[5:]

        self.logger.debug("%20s: %s" % ("[INSTANCE]", instance_string))
        self.logger.debug("%20s: %s" % ("[SPECIFIC_INFO]", specific_info))
        self.logger.debug("%20s: %f" % ("[RUNTIME_CUTOFF]", runtime_cutoff))
        self.logger.debug("%20s: %s" % ("[RUNLENGTH]", str(runlength)))
        self.logger.debug("%20s: %d" % ("[SEED]", seed))

        if instance_string not in self.inst_feat_dict:
            self.logger.info("Trying to find instance")
            instance_string = instance_string.split("/")[-1]

        if instance_string in self.inst_feat_dict:
            feat = self.inst_feat_dict[instance_string].reshape([1, -1])
            self.logger.debug("[FEAT]: [%s]" % ",".join([("%f" % f) for f
                                                         in feat.flatten()]))
            vec = SurrogateModel.convert_params_to_vec(param_str=params,
                                                       cs=self.cs,
                                                       encode=self.encode,
                                                       impute_with=self.impute_with)
            X = numpy.hstack([vec, feat]).astype(self._dtype)
            pred, additional = self.predict(model=self.model,
                                            logger=self.logger,
                                            X=X, cutoff=runtime_cutoff,
                                            quantile_seed=seed,
                                            quality=self.quality)
            self.logger.debug("[PRED]: %f" % pred)
            state = surrogate_remote.SUCCESS
        else:
            additional = "UNKNOWNINSTANCE: %s" % instance_string
            state = surrogate_remote.FAIL
            pred = None
        return state, pred, additional

    def send_answer(self, msg, state):
        self.logger.debug("[%s]:%s\n" % (state, msg))
        self.remote.send(state=state, msg=msg)
        self.remote.disconnect()

    def shutdown(self):
        self.logger.critical("\n" + "#"*80 + "\n..Shutting down..\n" + "#"*80 +
                             "\n\n")
        os.remove(self.portfile)
        del self.remote

    @staticmethod
    def predict(model, logger, X, quality, cutoff=-1.0, quantile_seed=0):
        if isinstance(model, external_rfr_predict.ExternalRFRPredict):
            pred = model.predict(X, logged=True)
        elif isinstance(model, external_rfr_quantile.ExternalRFRQuantile):
            pred = model.predict(X, seed=quantile_seed, num_samples=1)
        else:
            pred = model.predict(X)
        logger.debug("[log10(PRED)]: (%s) -> %s (%s)" % (str(X.shape),
                                                         str(pred),
                                                         str(len(pred))))
        pred = unwarp(pred, quality=quality)
        additional = "TRUE"
        if cutoff > 0:
            if pred > cutoff:
                additional = "CUTOFF"
                logger.critical("[WAR]: Predicted %f, which is higher than "
                                "cutoff %s" % (pred, cutoff))
                pred = cutoff
        return pred, additional
