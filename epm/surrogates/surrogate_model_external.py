'''
EPM -- empirical performance models

@author:     Katharina Eggensperger and Marius Lindauer

@copyright:  2015 AAD Group Freiburg. All rights reserved.

@license:    GPLv2

@contact:    {eggenspk,lindauer}@cs.uni-freiburg.de
'''

import cPickle
import logging
import numpy

from epm.surrogates import surrogate_model
from epm.models import external_rfr_predict


class SurrogateModelExtern(object):

    def __init__(self, pkl, debug=False):
        self.logger = logging.getLogger("SurrogateModelExtern")

        if debug:
            logging.basicConfig(level=logging.DEBUG)
        else:
            logging.basicConfig(level=logging.INFO)

        m, inst_feat_dict, cs = cPickle.load(file(pkl))
        self.model = m
        self.inst_feat_dict = inst_feat_dict
        self.cs = cs
        if isinstance(self.model, external_rfr_predict.ExternalRFRPredict):
            self.encode = False
        else:
            self.encode = True

    def predict_from_message(self, msg):
        if self.model is None:
            self.logger.critical("Model not yet loaded")
            return -1, "CRASHED", "MODELNOTLOADED"

        request = msg.split(" ")
        instance_string = request[0]
        specific_info = request[1]          # ignored
        runtime_cutoff = float(request[2])
        runlength = float(request[3])       # ignored
        seed = int(float(request[4]))
        params = request[5:]

        if instance_string not in self.inst_feat_dict:
            self.logger.critical("Instance is not known: %s" % instance_string)
            return -1, "CRASHED", "UNKNOWNINSTANCE"

        # Do a normal prediction
        feat = self.inst_feat_dict[instance_string].reshape([1, -1])
        self.logger.debug("[FEAT]: [%s]" % ",".join([("%f" % f) for f
                                                     in feat.flatten()]))
        vec = surrogate_model.SurrogateModel.convert_params_to_vec(
                param_str=params, cs=self.cs, encode=self.encode)
        self.logger.debug("[VEC]: [%s]" % ",".join([("%f" % v) for v
                                                    in vec.flatten()]))
        X = numpy.hstack([vec, feat]).astype(numpy.float32)
        pred, additional = surrogate_model.SurrogateModel.\
            predict(model=self.model, logger=self.logger, X=X,
                    cutoff=runtime_cutoff, quantile_seed=seed)
        return pred, additional