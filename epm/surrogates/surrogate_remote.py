'''
EPM -- empirical performance models

@author:     Katharina Eggensperger and Marius Lindauer

@copyright:  2015 AAD Group Freiburg. All rights reserved.

@license:    GPLv2

@contact:    {eggenspk,lindauer}@cs.uni-freiburg.de
'''

import socket

import logging

SUCCESS="SUCC"
FAIL="FAIL"

logger = logging.getLogger("Remote")


class SurrogateRemote(object):

    def __init__(self, ip, debug=False):
        self.ip_address = ip

        self.connected = False
        self._sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        self._conn = None
        self.port = None
        if debug:
            logger.setLevel(logging.DEBUG)

    def start_socket(self):
        success = True
        try:
            # Bind to port 0 -> OS will find free port
            # http://stackoverflow.com/questions/1365265/on-localhost-how-to-pick-a-free-port-number
            self._sock.bind((self.ip_address, 0))
            self._sock.listen(1)
            self.port = self._sock.getsockname()[1]
        except socket.error, msg:
            logger.debug("[FAILED]: %s\n" % str(msg))
            success = False
        logger.debug("[SUCCESS]: Running on port: %d", self.port)
        return success

    def __del__(self):
        if self._conn is not None:
            self._conn.close()
        self._sock.close()

    def connect(self, timeout=None):
        self._sock.settimeout(timeout)
        self._conn, addr = self._sock.accept()
        self._conn.settimeout(None)
        self.connected = True

    def disconnect(self):
        self._conn.close()
        self._conn = None
        self.connected = False

    def send(self, msg, state):
        assert self._conn is not None
        logger.debug("[SENDING] to [%s]: %s:%s" % (str(self._conn), state, msg))
        self._conn.sendall("%s:%s" % (state, msg))

    def receive(self):
        assert self._conn is not None
        data = self._conn.recv(8192)  # buffer size is 8192 bytes / 1024 kbytes
        logger.debug("[RECEIVING]: %s" % data)
        return data