'''
Created on Apr 14, 2015

@author: Katharina Eggensperger and Andre Biedenkapp
'''

import hashlib
import logging
import os
import time

import numpy

from epm.pcs.config_space import ConfigSpace
from epm.reader.feature_parser import FeatureParser
from epm.reader.performance_parser import PerformanceParser
from epm.reader.instance_parser import InstanceParser
from epm.reader.matrix_parser import MatrixParser
from epm.preprocessing.pre_feature import PreprocessingFeatures


def load_data(csv_file, feature_file, pcs_file, cutoff, downsample,
              normalize_parameters, instance_file=None, normalize_features=True,
              oneHot=True, par=1, debug=False, _dtype=numpy.float32,
              return_file_id=False, impute_with='def', quality=False):
    """
    reads in csv, feature and pcs file. Returns encoded_matrix, inst_feat_dict,
    inst_list, n_feats_used, perf_list, timeout_list, cen_list, (id_list)
    Does normalize features, impute non-active params, normalize data, and
    one-hot-encoding
    :param csv_file: file with configurations
           feature_file: file with instance features
           pcs_file: pcs file
           cutoff: cutoff for this experiment
           downsample: return only that many samples; value is also used as seed
           normalize_parameters: Whether to normalize parameters to be in [0,1]
           instance_file: file with instance subset
           normalize_features: normalize features (set to false, to allow
                                                   further data splitting
           oneHot: Perform one-hot encoding for configurations
           par: [IGNORED]
           debug: set logging level to debug
           _dtype: all returned values will have this dtype
           return_file_id: if set to true returns an additional list with file
                           ids for each entry
           impute_with: impute inactive params with 'def' or outlier
           quality: y is quality so timeout_list, cen_list are NONE
    :return:
    """
    logger = logging.getLogger("epm.experiment_utils.data_handling.load_data")
    if debug:
        logger.setLevel(logging.DEBUG)

    logger.info("Quality: %s" % str(quality))

    # Read performance data
    logging.info("Load csv: %s" % csv_file)
    start = time.time()
    pp = PerformanceParser(debug=debug, par=par, cutoff=cutoff, quality=quality)

    if not (isinstance(csv_file, list) or isinstance(csv_file, tuple)):
        csv_file = list([csv_file, ])

    if quality:
        config_list, perf_list, inst_list, id_list = \
            pp.read_data_from_multiple_files_with_id(files_=csv_file)
    else:
        config_list, perf_list, inst_list, timeout_list, cen_list, id_list = \
            pp.read_data_from_multiple_files_with_id(files_=csv_file)

    if downsample is not None and downsample < len(perf_list):
        logger.critical("DOWNSAMPLE from %d to %d" % (len(perf_list), downsample))
        rs = numpy.random.RandomState(downsample)
        idx = list(range(len(perf_list)))
        rs.shuffle(idx)
        idx = idx[:downsample]
        config_list = [config_list[i] for i in idx]
        inst_list = [inst_list[i] for i in idx]
        perf_list = [perf_list[i] for i in idx]
        id_list = [id_list[i] for i in idx]
        if not quality:
            timeout_list = [timeout_list[i] for i in idx]
            cen_list = [cen_list[i] for i in idx]

    dur = time.time() - start
    logger.info("Reading performance data took %5.3fsec" % dur)

    # Read features
    start = time.time()
    if feature_file is not None:
        logger.info("Load features: %s" % feature_file)
        fp = FeatureParser()
        inst_feat_dict, n_feats_used = fp.read_features(feature_file=feature_file,
                                                        n_feats=-1)
        # Normalize features to be within [0,1]
        if normalize_features:
            start = time.time()
            fpre = PreprocessingFeatures(inst_feats_dict=inst_feat_dict)
            inst_feat_dict = fpre.normalization(inst_feats_dict=inst_feat_dict)
            dur = time.time() - start
            logger.info("Normalizing features took %5.3fsec" % dur)
        else:
            logger.info("Do not normalize features")
    else:
        logging.info("Fake features")
        inst_feat_dict = dict()
        inst_set = set(inst_list)
        for i in inst_set:
            inst_feat_dict[i] = numpy.array([len(inst_feat_dict), ])
        n_feats_used = 1

    dur = time.time() - start
    logger.info("Getting features took %5.3fsec" % dur)

    # If we want to use a subset of instances given in a file
    if instance_file is not None:
        ip = InstanceParser()
        use_inst_list = ip.read_instances(instance_file)
        # Rebuild all lists
        tmp_config_list = list()
        tmp_perf_list = list()
        tmp_timeout_list = list()
        tmp_cen_list = list()
        tmp_inst_list = list()
        for idx, inst in enumerate(inst_list):
            if inst in use_inst_list:
                tmp_config_list.append(config_list[idx])
                tmp_perf_list.append(perf_list[idx])
                if quality:
                    tmp_timeout_list.append(timeout_list[idx])
                    tmp_cen_list.append(cen_list[idx])
                tmp_inst_list.append(inst_list[idx])
        logger.info("Ignore %d instances due to %s" % (len(perf_list) -
                                                       len(tmp_perf_list),
                                                       instance_file))
        if len(tmp_config_list) == 0:
            raise ValueError("No performances left, maybe wrong instance "
                             "file? %s" % instance_file)
        perf_list = tmp_perf_list
        config_list = tmp_config_list
        inst_list = tmp_inst_list
        if not quality:
            timeout_list = tmp_timeout_list
            cen_list = tmp_cen_list
            assert len(timeout_list) == len(perf_list) == len(config_list) == len(cen_list)
        else:
            assert len(perf_list) == len(config_list)

    else:
        logger.info("Use all instances")

    # Convert training data to matrix, impute nonactive params and normalize
    start = time.time()
    cs = ConfigSpace(pcs_file, normalize=normalize_parameters)
    config_matrix = numpy.zeros((len(config_list), cs._n_params),
                                dtype=_dtype)
    for indx, config in enumerate(config_list):
        config_vec = cs.convert_param_dict(config)
        imputed_vec = cs.impute_non_active(config_vec, value=impute_with)
        config_matrix[indx] = imputed_vec
    dur = time.time() - start
    logger.info("Converting data to matrix took %5.3fsec, dimensionality: %s" %
                (dur, str(config_matrix.shape)))

    # One-hot encode configuration matrix
    start = time.time()
    if oneHot:
        encoded_matrix = cs.encode(X=config_matrix, cat_list=cs._cat_size)
    else:
        encoded_matrix = config_matrix
    dur = time.time() - start
    logger.info("OneHot-Encoding data took %5.3fsec, dimensionality: %s" %
                (dur, str(encoded_matrix.shape)))
    logger.info("Finish: Data %s; Feat [%d, %d]" %
                (str(encoded_matrix.shape), len(inst_feat_dict.keys()),
                 len(list(inst_feat_dict.values())[0])))

    encoded_matrix = encoded_matrix.astype(dtype=_dtype)
    perf_list = numpy.array([_dtype(i) for i in perf_list], dtype=_dtype)

    if quality:
        cen_list = None
        timeout_list = None

    if return_file_id:
        return encoded_matrix, inst_feat_dict, inst_list, n_feats_used,\
               perf_list, timeout_list, cen_list, id_list
    else:
        return encoded_matrix, inst_feat_dict, inst_list, n_feats_used, \
               perf_list, timeout_list, cen_list


def loadMatlabData(pcs_file, feature_file, performance_file, config_file,
                   normalize_features=True, max_y=300, oneHot=True,
                   quality=False):
    """
    reads in csv, feature, performance and config file file. Returns X and y.
    Does normalize features, impute non-active params, normalize data, and
    one-hot-encoding
    :param csv_file: file with configurations
    
    :return:
    """

    if quality:
        raise ValueError("Reading quality data from matlab is not yet"
                         " implemented")

    logger = logging.getLogger("epm.experiment_utils.data_handling.loadMatlabData")

    # Read in features
    logging.info("Load features: %s" % feature_file)
    start = time.time()
    fp = FeatureParser()
    inst_feat_dict, n_feats_used = fp.read_features(feature_file=feature_file, n_feats=-1)
    dur = time.time() - start
    logger.info("Reading Features took %ssec" % dur)

    # Normalize features to be within [0,1]
    if normalize_features:
        start = time.time()
        fpre = PreprocessingFeatures(inst_feats_dict=inst_feat_dict)
        inst_feat_dict = fpre.normalization(inst_feats_dict=inst_feat_dict)
        dur = time.time() - start
        logger.info("Normalizing features took %5.3fsec" % dur)
    else:
        logger.info("Do not normalize features")

    # Read in performance data
    logging.info("Load performance_Matrix: %s" % performance_file)
    start = time.time()
    mp = MatrixParser()
    perf_list, timeout_list, cen_list, inst_list, best_perf, ic = mp.read_performance_matrix(
        performance_file, matrix_captime=max_y)

    dur = time.time() - start
    logger.info("Reading performance data took %ssec" % dur)
    
    logging.info("Load config_matrix: %s" % config_file)
    start = time.time()
    config_list = mp.read_config(config_file, ic[0])
    dur = time.time() - start
    logger.info("Reading config data took %ssec" % dur)


    # Convert training data to matrix, impute nonactive params and normalize
    start = time.time()
    cs = ConfigSpace(pcs_file, normalize=True)
    config_matrix = numpy.zeros((len(config_list), cs._n_params))
    for indx, config in enumerate(config_list):
        config_vec = cs.convert_param_dict(config)
        # mean imputation
        imputed_vec = cs.impute_non_active(config_vec, value="def")
        config_matrix[indx] = imputed_vec
    dur = time.time() - start
    logger.info("Converting data to matrix took %ssec, dimensionality: %s" %
                (dur, str(config_matrix.shape)))

    # One-hot encode performance matrix
    start = time.time()
    if oneHot:
        encoded_matrix = ConfigSpace.encode(X=config_matrix,
                                            cat_list=cs._cat_size)
    else:
        encoded_matrix = config_matrix
    dur = time.time() - start
    logger.info("Encoding data took %ssec, dimensionality: %s" %
                (dur, str(encoded_matrix.shape)))

    encoded_matrix = encoded_matrix.astype(numpy.float32)
    perf_list = list([numpy.float32(i) for i in perf_list])

    return encoded_matrix, inst_feat_dict, inst_list, n_feats_used, perf_list, \
           timeout_list, cen_list, best_perf


def partition_data(data_matrix, inst_list, rs=None, debug=True):
    """
    split data into four parts
    :param data_matrix: encoded data matrix with configs
    :param inst_list: list of instances
    :param seed: optional, seed for numpy.random
    :param debug: optional, if True don't retry
    :return: idx for upper_left, upper_right, lower_left, lower_right
    """
    logger = logging.getLogger("epm.experiment_utils.data_handling."
                               "partition_data")

    if rs is None:
        rs = numpy.random.RandomState()

    unique_config = set()
    for row in data_matrix:
        hasher = hashlib.md5()
        hasher.update(row.view(numpy.float32))
        unique_config.add(hasher.hexdigest())
    unique_config = list(unique_config)

    rs.shuffle(unique_config)
    logger.info("Found %d (%d) unique configs" % (len(unique_config),
                                                  data_matrix.shape[0]))

    # get unique instances
    unique_instance = numpy.unique(inst_list)
    rs.shuffle(unique_instance)
    logger.info("Found %d (%d) unique instances" % (len(unique_instance),
                                                    len(inst_list)))

    # Split into two equal parts
    unique_instance = numpy.array_split(unique_instance, 2)
    #unique_config = numpy.array_split(unique_config, 2)
    unique_config = [unique_config[:int(len(unique_config)/2)],
                     unique_config[int(len(unique_config)/2):]]
    # (conf, inst)
    # 00 01
    # 10 11
    indices = list([[[], []], [[], []]])

    # Iterate over matrix and instances
    for idx, row in enumerate(data_matrix):
        #c_hash = row.dot(random_matrix)
        hasher = hashlib.md5()
        hasher.update(row.view(numpy.float16))
        c_hash = hasher.hexdigest()
        if c_hash in unique_config[0]:
            if inst_list[idx] in unique_instance[0]:
                indices[0][0].append(idx)
            elif inst_list[idx] in unique_instance[1]:
                indices[0][1].append(idx)
            else:
                raise ValueError("Could not find instance %s" % inst_list[idx])
        elif c_hash in unique_config[1]:
            if inst_list[idx] in unique_instance[0]:
                indices[1][0].append(idx)
            elif inst_list[idx] in unique_instance[1]:
                indices[1][1].append(idx)
            else:
                raise ValueError("Could not find instance %s" % inst_list[idx])
        else:
            raise ValueError("Could not find config: %s, idx: %d, hash: %s" %
                             (str(row), idx, c_hash))

    # Do some checks
    if len(indices[0][0]) == 0 or len(indices[1][0]) == 0 or \
                    len(indices[0][1]) == 0 or len(indices[1][1]) == 0:
        raise ValueError("Could not split, retry with different seed")
    else:
        return indices


def separate_data_with_bools(data_matrix, inst_list, perf_list, succ_list):
    logger = logging.getLogger("epm.experiment_utils.data_handling."
                               "separate_data_with_bools")

    # Remove censored data
    start = time.time()

    succ_list = numpy.array(succ_list)
    true_encoded_matrix = data_matrix[succ_list, :]
    true_perf_list = [perf_list[i] for i, suc in enumerate(succ_list) if suc]
    true_inst_list = [inst_list[i] for i, suc in enumerate(succ_list) if suc]

    false_encoded_matrix = data_matrix[~succ_list, :]
    false_perf_list = [perf_list[i] for i, suc in enumerate(succ_list) if not suc]
    false_inst_list = [inst_list[i] for i, suc in enumerate(succ_list) if not suc]

    dur = time.time() - start
    logger.info("Splitting data took %ssec, true dim: %s, false dim: %s" %
                (dur, str(true_encoded_matrix.shape),
                 str(false_encoded_matrix.shape)))

    return true_encoded_matrix, true_inst_list, true_perf_list, \
           false_encoded_matrix, false_inst_list, false_perf_list


def build_data(data_matrix, inst_list, inst_feat_dict, n_feats):
    # builds numpy array from various data structures
    train_X = numpy.zeros([data_matrix.shape[0],
                           data_matrix.shape[1] + n_feats],
                          dtype=numpy.float32)
    for i in range(data_matrix.shape[0]):
        vec = numpy.hstack((data_matrix[i, :], inst_feat_dict[inst_list[i]]))
        train_X[i] = vec
    return train_X


def warp(x, quality):
    if quality:
        return x
    else:
        return numpy.log10(x)


def unwarp(x, quality):
    if quality:
        return x
    else:
        return numpy.power(10, x)