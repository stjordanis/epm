'''
Created on Mar 25, 2015

@author: manju
'''
import logging
import time

import numpy
import scipy.stats as stats

from epm.experiment_utils.data_handling import warp, unwarp

class FastImputorY(object):
    """
        impute right-censored y
    """

    def __init__(self, debug=False, max_iter=10):
        """
            Constructor
        """
        self.logger = logging.getLogger("FastImputorY")

        if debug:
            self.logger.setLevel(logging.DEBUG)
        else:
            self.logger.setLevel(logging.ERROR)

        self.max_iter = max_iter
        self.debug = debug

        # Now this is accessible
        self.used_it = None
        self.change_traj = list()
        self.data_traj = list()
        self._dtype = numpy.float32

    def impute_y(self, y, is_censored, cutoff, configs_list, inst_list,
                 inst_feature_dict, model, par=1, log=False):
        """
            impute right-censored y
            Args:
                y: vec of y values
                is_censored: boolean list indicating which y value is censored
                configs_list: configuration vectors corresponding to y
                cutoff : maximal value of y
                inst_list: instances corresponding to y
                inst_feature_dict: mapping instance name to normalized features
                model: provides fit(X,y) and predict_mean_std(X)
                par: PAR-value, treat values > cutoff as cutoff*par
                log: predict logged values
        """

        # censored indexes
        censored_idx = []
        uncensored_idx = []
        for indx, cen in enumerate(is_censored):
            if cen:
                censored_idx.append(indx)
            else:
                uncensored_idx.append(indx)

        # prepare uncensored data
        y_uncen = numpy.array([y[indx] for indx in uncensored_idx],
                              dtype=self._dtype)
        configs_ls_uncen = numpy.array([configs_list[indx] for indx
                                        in uncensored_idx], dtype=self._dtype)
        inst_ls_uncen = [inst_list[indx] for indx in uncensored_idx]
        feats_uncen = numpy.array([inst_feature_dict[inst] for inst
                                   in inst_ls_uncen], dtype=self._dtype)

        X_uncen = numpy.concatenate((configs_ls_uncen, feats_uncen), axis=1)

        # prepare censored data
        y_cen = numpy.array(list(y[indx] for indx in censored_idx),
                            dtype=self._dtype)
        conf_ls_cen = numpy.array([configs_list[indx] for indx in censored_idx],
                                  dtype=self._dtype)
        inst_list_cen = list(inst_list[indx] for indx in censored_idx)
        feats_cen = numpy.array([inst_feature_dict[inst] for inst
                                 in inst_list_cen], dtype=self._dtype)

        X_cen = numpy.concatenate((conf_ls_cen, feats_cen), axis=1)

        new_ys = list(self.raw_impute_arrays(X_uncen=X_uncen, y_uncen=y_uncen,
                                             X_cen=X_cen, y_cen=y_cen, par=par,
                                             cutoff=cutoff, model=model, log=log))
        y_imp = list([new_ys.pop(0) if cen else y[idx]
                      for idx, cen in enumerate(is_censored)])

        return y_imp

    def raw_impute_arrays(self, X_uncen, y_uncen, X_cen, y_cen, model, cutoff,
                          par=1, log=False, change_threshold=0.01,
                          store_info=False, impute_with="def"):
        start_imp = time.time()
        # Some checks before we start
        if X_uncen.dtype != self._dtype:
            raise ValueError("X_uncen has dtype %s, expected %s" %
                             (str(X_uncen.dtype), self._dtype))
        if y_uncen.dtype != numpy.float32:
            raise ValueError("y_uncen has dtype %s, expected %s" %
                             (str(y_uncen.dtype), self._dtype))
        if X_cen.dtype != numpy.float32:
            raise ValueError("X_cen has dtype %s, expected %s" %
                             (str(X_cen.dtype), self._dtype))
        if y_cen.dtype != numpy.float32:
            raise ValueError("y_cen has dtype %s, expected %s" %
                             (str(y_cen.dtype), self._dtype))
        if log:
            self.logger.debug("Using log for fitting")
            y_uncen = warp(y_uncen, quality=False)
            y_cen = warp(y_cen, quality=False)
            cutoff = warp(cutoff, quality=False)
            par = warp(par, quality=False)
            threshold = cutoff + par
        else:
            threshold = cutoff * par

        self.logger.info("Start imputation with cutoff %f, par %f, "
                         "threshold %f" % (cutoff, par, threshold))

        # first learn model without censored data
        m = None
        m = model()

        if impute_with == "outlier":
            # We have to change type array here
            m.change_type_array_to_outlier_imputation()

        m.fit(X_uncen, y_uncen)
        self.logger.debug("Going to impute y-values with %s" % str(m))

        new_ys = None  # define this, if imputation fails
        self.used_it = -1

        it = 0
        change = 0

        if store_info:
            # Store initial values
            if log:
                self.data_traj.append(unwarp(list(y_cen), quality=False))
            else:
                self.data_traj.append(list(y_cen))

        while True:
            self.logger.debug("Iteration %d of %d" % (it, self.max_iter))

            # predict censored y values
            y_mean, y_stdev = m.predict_mean_std(X_cen)
            del m

            assert type(y_mean) == numpy.ndarray == type(y_stdev)
            assert len(y_mean.shape) == 1 == len(y_stdev.shape)
            assert y_mean.dtype == numpy.float32
            assert y_stdev.dtype == numpy.float32
            new_ys = [stats.truncnorm.stats(a=(y_cen[index] - y_mean[index]) / y_stdev[index],
                                            b=(numpy.inf - y_mean[index]) / y_stdev[index],
                                            loc=y_mean[index],
                                            scale=y_stdev[index],
                                            moments='m') for index in range(len(y_cen))]
            new_ys = numpy.array(new_ys).astype(numpy.float32)

            # lower all values that have already been higher than threshold
            # new_ys[y_cen.flatten() >= cutoff] = threshold

            # Replace all nans with threshold
            self.logger.critical("Going to replace %d nan-value(s) with "
                                 "threshold" %
                                 sum(numpy.isfinite(new_ys) == False))
            new_ys[numpy.isfinite(new_ys) == False] = threshold

            if it > 0:
                if log:
                    change = numpy.mean(
                            abs(unwarp(new_ys, quality=False)) -
                            unwarp(y[y_uncen.shape[0]:], quality=False) /
                            abs(unwarp(y[y_uncen.shape[0]:], quality=False)))
                else:
                    change = numpy.mean(abs(new_ys - y[y_uncen.shape[0]:]) /
                                        y[y_uncen.shape[0]:])

            # lower all values that are higher than threshold
            new_ys[new_ys >= threshold] = threshold

            if store_info:
                self.change_traj.append(change)
                if log:
                    self.data_traj.append(unwarp(new_ys, quality=False))
                else:
                    self.data_traj.append(new_ys)

            self.logger.debug("Change: %f" % change)

            X = numpy.concatenate((X_uncen, X_cen)).astype(numpy.float32)
            y = numpy.concatenate((y_uncen, new_ys)).astype(numpy.float32)

            if change > change_threshold or it == 0:
                start = time.time()
                m = model()
                if impute_with == "outlier":
                    # Also, we have to change type array here
                    m.change_type_array_to_outlier_imputation()
                m.fit(X, y)
                dur = time.time() - start
                self.logger.debug("Model training took: %fsec" % dur)
            else:
                break

            it += 1
            if it > self.max_iter:
                break
        self.used_it = it-1
        dur_imp = time.time() - start_imp
        self.logger.info("Imputation: %fsec; %d/%d iterations, last_change=%f)" %
                     (dur_imp, self.used_it, self.max_iter,
                      change))

        new_ys = numpy.array(new_ys, dtype=numpy.float32)
        new_ys[new_ys >= threshold] = threshold
        if log:
            new_ys = unwarp(new_ys, quality=False)

        assert numpy.isfinite(new_ys).all(), "Imputed values are not finite, " \
                                             "%s" % str(new_ys)
        return new_ys
