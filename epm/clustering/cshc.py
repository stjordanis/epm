'''
Created on Apr 1, 2015

@author: marius lindauer
'''

import numpy
import sys
import random
import logging
import functools

from epm.utils.parallelizer import Parallelizer
from argparse import ArgumentError

class Node(object):
    
    def __init__(self):
        
        self.children = []
        self.split_crit = None
        self.instances = None
        self.loss = None
        

    def print_tree(self, depth=0):
        
        if self.split_crit:
            logging.info("%sSplit: %s" %("".join([" "]*depth), str(self.split_crit)))
            for c in self.children:
                c.print_tree(depth=depth+2)
        else:
            logging.info("%s#Insts: %d" %("".join([" "]*depth),len(self.instances)))

class CSHC(object):
    '''
        cost sensitive hierarchical clustering
    '''


    def __init__(self, cpu_cores = 1):
        '''
        Constructor
        '''
        
        self._RANDOM_SPLITS = 1000
        
        self._THREADS = cpu_cores #4
        
        self._inst_perf = {}
        
        
    def cluster(self, inst_feat_dict, optimizer, model, pcs, min_cluster_size=10, max_clusters=8, split=2):
        '''
            cluster instances of inst_feat_dict into subsets 
            such that each split is selected according to the maximal agreement on the optimal configuration;
            using random axis parallel splits
            :param inst_feat_dict: dictionary instance name -> features
            :param optimizer: instance of optimizer package
            :param model: surrogate model that provides predict function
            :param pcs: instance of ConfigSpace
            :param min_cluster_size: minimal size of a cluster
            :param max_clusters: maximal size of clusters
            :param split: 1 -> axis parallel or 2 -> random plains 
            :return: triple of (instances, loss, node), root_node
        '''
        
        self.predict_func = model.predict
        
        root_node = Node()
        root_node.instances = inst_feat_dict.keys()
        root_node.loss = sys.maxint
        
        clusters = [(inst_feat_dict.keys(), sys.maxint, root_node)]
        changed = True
        
        partial_loss_function = functools.partial(self._get_loss_par, inst_feat_dict, optimizer, pcs )
        
        par = Parallelizer(self._THREADS)
        
        logging.debug("Running ILS on each inst")
        res = par.par_map(partial_loss_function, map(lambda x : [x], clusters[0][0]))
        logging.debug("Finished running ILS on each inst")
        
        self._inst_perf = dict((inst[0], res_) for res_, inst in res)
        
        logging.debug(len(res))
        logging.debug(len(self._inst_perf))
        logging.debug(len(inst_feat_dict))
        
        #=======================================================================
        # for inst in clusters[0][0]:
        #     inst = inst
        #     self._inst_perf[inst] = self._get_loss([inst], inst_feat_dict, optimizer, pcs)
        #     logging.debug("%s: %f" %(inst, self._inst_perf[inst]))
        #=======================================================================
        
        while len(clusters) < max_clusters and changed:
            changed = False
            for id, c in enumerate(clusters):
                c = c[0]
                inst_feat_dict_c = dict((inst, inst_feat_dict[inst]) for inst in c)
                best_split, loss_pair, best_split_crit = self._split(inst_feat_dict_c, optimizer, model, pcs, min_cluster_size, split)
                
                if len(best_split) > 1:
                    changed = True
                    break
            if changed:
                c = clusters.pop(id)
                parent_node = c[2]
                parent_node.instances = None
                parent_node.split_crit = best_split_crit
                left = Node()
                left.instances = best_split[0]
                left.loss = loss_pair[0]
                right = Node()
                right.instances = best_split[1] 
                right.loss = loss_pair[1]
                parent_node.children = (left, right)
                clusters.append((best_split[0], loss_pair[0], left))
                clusters.append((best_split[1], loss_pair[1], right))
            clusters.sort(key=lambda x : x[1], reverse=True)
            
            logging.debug(map(lambda x: len(x[0]), clusters))
            logging.debug(map(lambda x: x[1], clusters))
            
        logging.debug("Quality of clusters (avg): %.2f" %( sum(map(lambda x: x[1], clusters))/len(clusters)))
        return clusters, root_node
            
    
    def _split(self, inst_feat_dict, optimizer, model, pcs, min_cluster_size=10, split=1):
        '''
            cluster instances of inst_feat_dict into subsets 
            such that each split is selected according to the maximal agreement on the optimal configuration;
            using random (axis-parallel) splits 
            
            :param inst_feat_dict: dictionary instance name -> features
            :param optimizer: instance of optimizer package
            :param model: surrogate model that provides predict function
            :param pcs: instance of ConfigSpace
            :param min_cluster_size: minimal size of a cluster 
            :param split: 1 -> axis parallel or 2 -> random plains 
            :return: list of instance lists
        '''
        
        logging.debug("Input size: %d" %(len(inst_feat_dict)))
        if len(inst_feat_dict) <= min_cluster_size:
            logging.debug("Too small size: %d" %(len(inst_feat_dict)))
            return [inst_feat_dict.keys()], None, None
        
        instances = inst_feat_dict.keys()
        
        feat_matrix = numpy.array(inst_feat_dict.values())
        max_values = numpy.max(feat_matrix, axis=0)
        mean_values = numpy.mean(feat_matrix, axis=0)
        n_feat = feat_matrix.shape[1]
            
        # random splits to find a subsets with maximal configuration agreement
        partial_split_function = functools.partial(self._random_split, split, instances, inst_feat_dict, n_feat, mean_values, max_values, optimizer, pcs)
        par = Parallelizer(self._THREADS)
        
        res = par.par_map(partial_split_function, range(0, self._RANDOM_SPLITS))
        
        res.sort(key = lambda x: x[0])
        
        best_split = res[0][2]
        best_loss_values = res[0][1]
        best_split_crit = res[0][3]
        
        logging.debug(best_split)
                
        if not (best_split[0] and best_split[1]): # reject empty split
            logging.debug("Return (without split): %d" %(len(inst_feat_dict)))
            return [inst_feat_dict.keys()], None, None

        logging.debug("%d - %d" %(len(best_split[0]), len(best_split[1])))                     
        logging.debug("Return (with split)")
        #set_1.extend(set_2)
        return best_split, best_loss_values, best_split_crit
    
        
    def _random_split(self, split, instances, inst_feat_dict, n_feat, mean_values, max_values, optimizer, pcs, pseudo_arg=None):
        # random split by random instance and random features
        '''
            splits instances into two sets and returns these sets and the corresponding split criterion value
            :param split: 1 -> axis parallel or 2 -> random plains
            :param instances: list of instances
            :param inst_feat_dict: dictionary instance name -> features
            :param n_feat: number of instance features
            :param mean_values: means of instance features (vector)
            :param max_values: max values of instance features (vector)
            :param optimizer: instance of optimizer package
            :param pcs: instance of ConfigSpace
            :return: loss of split, loss of each subset, tuple of two instance sets, tuple of split (depends on split policy)
        '''
        
        split_crit = None
        
        if split == 1:
            rand_inst = random.choice(instances)
            feat_vec = inst_feat_dict[rand_inst]
            split_index = random.randint(0, len(feat_vec) - 1)
            split_value = feat_vec[split_index]
            split_crit = (split_index, split_value)
        elif split == 2:
            # stdev = 1
            support_vector = numpy.random.randn(n_feat) + mean_values
            support_vector[max_values == 0] = 0
            dir_vector = numpy.random.randn(n_feat)
            split_crit = (support_vector, dir_vector)
        else:
            raise ValueError("unknown split value (%d)" %(split))
        
        set_1 = []
        set_2 = []
        
        for inst in instances:
            if split == 1:
                if inst_feat_dict[inst][split_index] >= split_value:
                    set_1.append(inst)
                else:
                    set_2.append(inst)
            elif split == 2:
                if (numpy.dot ((inst_feat_dict[inst] - support_vector),  dir_vector)) > 0:
                    set_1.append(inst)
                else:
                    set_2.append(inst)
            else:
                raise RuntimeError("split method not implemented (1 or 2 are legal values)")
                
        if not (set_1 and set_2): # reject empty splits
            logging.debug("empty split")
            return sys.maxint, None, (None,None), None
        
        logging.debug("Split into %d vs %d instances" %(len(set_1), len(set_2)))        
        
        
        loss_set_1,_ = self._get_loss_par(inst_feat_dict, optimizer, pcs, set_1)
        loss_set_2,_ = self._get_loss_par(inst_feat_dict, optimizer, pcs, set_2)
        
        logging.debug("set1: %.2f - %.2f" %(loss_set_1, sum([self._inst_perf[inst] for inst in set_1])))
        logging.debug("set2: %.2f - %.2f" %(loss_set_2, sum([self._inst_perf[inst] for inst in set_2])))
        
        loss_values = (loss_set_1 - sum([self._inst_perf[inst] for inst in set_1]),
                          loss_set_2 - sum([self._inst_perf[inst] for inst in set_2]))
        loss = sum(loss_values)
        
        return loss, loss_values, (set_1, set_2), split_crit
    
    def _get_loss_par(self, inst_feat_dict, optimizer, pcs, insts):
        '''
            get loss of choosing a per-instance-set configuration in comparison to a per-instance configuration
        '''
    
        set_feats = numpy.array([inst_feat_dict[i] for i in insts])
        _, per_set_y = optimizer.optimize(self.predict_func, pcs, set_feats)

        return per_set_y, insts
