'''
Created on Mar 23, 2015

@author: lindauer
'''

import logging
import numpy
import time
import random

class IteratedLocalSearch(object):
    '''
         iterated local search to optimize a function over a given parameter configuration space
    '''


    def __init__(self, restarts=4, max_steps=20, max_neighbors=200):
        '''
        Constructor
        '''
        random.seed(12345)
        
        self.__RESTARTS = restarts
        self.__MAX_STEPS = max_steps
        self.__MAX_NEIGHBORS = max_neighbors

        # very fast and inaccurate
        
        #======================================================================
        # self.__RESTARTS = 1
        # self.__MAX_STEPS = 5
        # self.__MAX_NEIGHBORS = 5
        #======================================================================

        # medium 
        
        #=======================================================================
        # self.__RESTARTS = 4
        # self.__MAX_STEPS = 20
        # self.__MAX_NEIGHBORS = 20
        #=======================================================================
                 
        #=======================================================================
        # self.__RESTARTS = 10
        # self.__MAX_STEPS = 50
        # self.__MAX_NEIGHBORS = 20
        # 
        #=======================================================================
    def optimize(self, func, pcs, feats, init_x=None):
        '''
            find a configuration in pcs (ConfigSpace) that optimizes func(x);
            starts at init if given or else with pcs.get_default_config_dict()
            :param func: function with input vec x that returns the predicted performance of x
            :param pcs: ConfigSpace object
            :param feats: matrix of instance features
            :param init_x: initial x (if None, use default config)
            :return: tuple of (i) optimized x on <func>  given <feats> and (ii) its performance
        '''
        
        if not init_x:
            init_x = pcs.get_default_config_dict()
            init_x = pcs.convert_param_dict(init_x)
            #init_x = pcs.impute_non_active(init_x, "mean")
            #init_x = pcs.encode(numpy.array([init_x]))[0]
        
        #self.__MAX_NEIGHBORS = max(len(init_x), self.__MAX_NEIGHBORS)
            
        # aggregate over func over instance feature vectors
        #opt_func = lambda x: sum(map(lambda f: func(numpy.concatenate((pcs.encode(numpy.array([pcs.impute_non_active(x,"mean")]))[0],f))), feats)) 
            
        def opt_func2(x):
            #x = pcs.encode(numpy.array([pcs.impute_non_active(x,"mean")]))[0]
            x = pcs.impute_non_active(x,"def")
            X = numpy.repeat(numpy.reshape(x, (1,x.shape[0])), feats.shape[0], axis=0)
            X = numpy.concatenate((X,feats), axis=1)
            return sum(func(X))
                        
        best_y = opt_func2(init_x)
        best_x = init_x
        logging.log(5, "Init y: %.2f " %(best_y))
        
        for r in xrange(self.__RESTARTS):
            #logging.debug("ILS %d Restart" %(r))
            if r == 0: # start with init in first iteration
                x = numpy.copy(best_x)   
                y = best_y
            else: # next iteration at new random x
                x = pcs.get_random_config_vector()
                y = opt_func2(x)
            for _ in xrange(self.__MAX_STEPS):
                updated = False
                neighbors = pcs.get_all_on_ex_neighbors(x)
                random.shuffle(neighbors)
                neighbors = neighbors[:self.__MAX_NEIGHBORS]
                for new_x in neighbors: # selection: first improving neighbor
                    #logging.debug("\n".join(map(str,x[9:11])))
                    #new_x = pcs.get_random_neighbor(x)
                    #logging.debug("\n".join(map(str,new_x[9:11])))
                    new_y = opt_func2(new_x)
                    logging.log(5, "New y: %.5f" %(new_y))
                    if new_y < y:
                        y, x = new_y, new_x
                        updated = True
                        #logging.debug("New y: %.4f" %(y))
                        #logging.debug("New y:x: %.2f:%s" %(y, str(x)))
                        break
                if not updated: 
                    break
            if y < best_y:
                best_y, best_x = y, x
        logging.log(5,"Best y:x %.4f %s" %(best_y, str(best_x)))
        
        return best_x, best_y
        
        
        
        
        