'''
Created on Mar 23, 2015

@author: lindauer
'''

import logging
import numpy
import random
from scipy.optimize import minimize

class LBFGSB(object):
    '''
         L-BFGS-B to optimize a function over a given parameter configuration space
    '''


    def __init__(self, max_iter = 1000):
        '''
        Constructor
        '''
        random.seed(12345)
        
        self.__MAX_ITER = max_iter

    def optimize(self, func, pcs, feats, init_x=None):
        '''
            find a configuration in pcs (ConfigSpace) that optimizes func(x);
            starts at init if given or else with pcs.get_default_config_dict()
            :param func: function with input vec x that returns the predicted performance of x
            :param pcs: ConfigSpace object
            :param feats: matrix of instance features
            :param init_x: initial x (if None, use default config)
            :return: tuple of (i) optimized x on <func>  given <feats> and (ii) its performance
        '''
        
        if not init_x:
            init_x = pcs.get_default_config_dict()
            init_x = pcs.convert_param_dict(init_x)
            
        def opt_func2(x):
            #x = pcs.encode(numpy.array([pcs.impute_non_active(x,"mean")]))[0]
            x = pcs.impute_non_active(x,"def")
            X = numpy.repeat(numpy.reshape(x, (1,x.shape[0])), feats.shape[0], axis=0)
            X = numpy.concatenate((X,feats), axis=1)
            return sum(func(X))
                        
        best_y = opt_func2(init_x)
        logging.log(5, "Init y: %.2f " %(best_y))
        
        res = minimize(fun=opt_func2, x0=init_x, options={"maxiter": self.__MAX_ITER})
        best_x = res.x
        best_y = opt_func2(best_x)
        
        logging.log(5,"Best y:x %.4f %s" %(best_y, str(best_x)))
        
        return best_x, best_y
        
        
        
        
        